## Goal

The goal of this exercise is to run and interact with the Pet Store API inside a container.

## Instructions

We want to mount the petstore-node directory into the container. We also want to give our
container a name so it's easier to interact with.

1.  Open two terminal windows, both in the `docker-intro/exercise1` directory.

1.  We will modify the docker run command we've been using:
    `docker run --rm -it [options] kata:node6 /bin/ash`

    1.  Use the `--name` option to name the container `kata`
    
    1.  Use the `--volume <host_absolute_path>:<container_absolute_path>` option to mount the
        `petstore-node` directory to `/petstore` inside the container
    
    1.  Execute the command to open a shell in the container.

1.  Now, you should be able to run `ls /petstore` in the container and see the API files.

1.  In the second terminal, on your host machine, create the file
    `docker-intro/exercise1/petstore-node/hello.txt` with the content:

    ```
    Hello, Docker!
    ```
    
1.  In the container, you can see the file you just created.

    1.  Run `ls /petstore` again and you'll see `hello.txt`!
    
    1.  Run `cat /petstore/hello.txt` to see the content.

1.  In the container, run these commands to start the server:

    ```bash
    cd /petstore
    npm start
    ```

1.  In the second terminal, run the command `docker exec -it kata /bin/ash` to open a
    new shell in your container.

1.  Now, use `curl` to interact with the application.
    -   `curl localhost:8080/v2/store/order/1`
    -   `curl localhost:8080/v2/pet/1`
