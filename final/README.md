# Optimized Dockerfile

This directory has optimized the Dockerfile and directory structure to minimize the
number of layers created when the image is built. The downside to this approach is
that any change to the Pet Store API or `entrypoint.sh` will result in every layer,
including the `npm install` being rebuilt rather than loaded from cache.

When you're developing an image, it's a good idea to break statements apart so you
can cache working layers while debugging problems with the other layers in the file.

## Directory Structure

-   `dockerfiles`

    This directory contains all the files we'll be copying into the image, with
    a directory structure mirroring the root filesystem so our files are placed
    in the desired locations.
    
    -   `entrypoint.sh`
    
    The entrypoint script that dynamically configures the API host name in the
    documentation. This is installed in the container as `/entrypoint.sh`.
    
    -   `petstore`
    
    The Pet Store API application (`petstore-node`). This is be installed in the container
    as `/petstore`.
    
-   `.dockerignore`

    A `.gitignore`-style file that tells `docker build` which files to exclude from
    the build context.
    
-   `Dockerfile`

    The optimized Dockerfile.
    
-   `README.md`

    This file.
    
## Dockerfile

`FROM` must be the first directive found in a Dockerfile. It tells `docker build` which base
image to use.

```dockerfile
FROM node:6-alpine
```

`MAINTAINER` provides information about the person or organization that maintains this image,
typically including an email address or other method of contact.

```dockerfile
MAINTAINER Gordon Shankman <gordon.shankman@bti360.com>
```

`EXPOSE` lets Docker know the container opens a port that can be mapped to the host system
or used for communication when other containers link to this one.

```dockerfile
EXPOSE 8080
```

`ENV` sets one or more environment variables. In this case, we're setting the `API_SERVER`
that is used to configure the "Try it out!" section of the API docs.

```dockerfile
ENV API_SERVER=localhost
```

`ADD` is used to download a file from the internet to a particular location in the container

```dockerfile
ADD https://github.com/Yelp/dumb-init/releases/download/v1.2.0/dumb-init_1.2.0_amd64 /usr/local/bin/dumb-init
COPY dockerfiles/ /
```

`WORKDIR` changes the working directory in the container for all subsequent directives.

```dockerfile
WORKDIR /petstore
```

This `RUN` directive uses command chaining (`x && y`) to ensure `dumb-init` and `entyrpoint.sh`
are executable, install `npm` dependencies, and change ownership of the `/petstore` directory
to the `node` user and group. If any of the commands fails, Docker will automatically stop the
build process.

```dockerfile
RUN chmod +x /usr/local/bin/dumb-init /entrypoint.sh && \
    npm install && \
    chown -R node:node /petstore
```

The `USER` directive changes the active user for all subsequent commands, including those in
child containers and the `ENTRYPOINT`/`CMD` execution. Here we're using the `node` user that
is defined by our base container, `node:6-alpine`.

```dockerfile
USER node
```

`ENTRYPOINT` and `CMD` work together to determine the default startup command for the container.
The `ENTRYPOINT` defines a command that is always run at startup, using the `CMD` or `docker run`
arguments as its values.  This section always runs `/entrypoint.sh` with `dumb-init`, using
default command line arguments of `npm start`.

```dockerfile
ENTRYPOINT ["/usr/local/bin/dumb-init", "--", "/entrypoint.sh"]
CMD ["npm", "start"]
```
