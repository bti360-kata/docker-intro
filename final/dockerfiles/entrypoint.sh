#!/bin/sh

if [ -n "${API_SERVER}" ]; then
    sed -i -e "s/^host:.*/host: \"${API_SERVER}\"/" /petstore/api/swagger.yaml
fi

$*
