## Goal

The goal of this exercise is to build a container that includes all dependencies, automatically launches
our Pet Store application, and makes the application accessible outside of the container.

## Instructions

1.  Open the `exercise2/Dockerfile` in a text editor.

1.  Add an `EXPOSE` directive to tell Docker that the image has an application running on port `8080`.

    ```dockerfile
    EXPOSE <port>
    ```

1.  Next, use the `WORKDIR` directive to change the current working directory to `/petstore`. All
    commands after this directive will use the new working directory as their starting point.
    
    ```dockerfile
    WORKDIR <container_path>
    ```
 
1.  Now, use the `RUN` directive to execute the command `npm install`.
    
    ```dockerfile
    RUN <cmd>
    ```

1.  Finally, we'll use `CMD` to configure the container to run `npm start` on startup.

    ```dockerfile
    CMD ["npm", "start"]
    ```

1.  Save and close the `Dockerfile`.

1.  Go back to your terminal window and build your container! Make sure you're
    in the `exercise2` directory.

    ```bash
    docker build -t petstore:api .
    ```

1.  Now, we'll add the `-p` option to `docker run`. Similarly to the `-v` option and volumes,
    `-p` maps ports between the host and container. Run your container, mapping port `10080`
    on the host to port `8080` on the container.
    
    ```bash
    docker run --rm -p <host_port>:<container_port> petstore:api
    ```

1.  Open <http://localhost:10080/docs> in your browser!

1.  You can also test the API from your local machine with `curl` (or a browser if you don't have
    curl installed):
    
    ```bash
    curl localhost:10080/v2/store/order/1
    curl localhost:10080/v2/pet/1
    ```
