## Goal

The goal of this exercise is to make your container configurable at runtime so you can run locally,
deploy to multiple environments, and distribute the container to others.

## ENV

Docker provides an `ENV` directive that allows you to define environment variables with
default values in your Dockerfile.

The syntax is:

```dockerfile
ENV <NAME>=<DEFAULT>
```

You can also override environment variables at runtime with `docker run`'s `-e <NAME>=<VALUE>` option.

For this exercise, the `entrypoint.sh` script uses the `API_SERVER` variable.

## Instructions

-   Use the `COPY`, `ENV`, and `RUN` directives to:
    -   Include `entrypoint.sh` in the root directory of your image and make sure it is executable;
    -   Update the image so `dumb-init` calls `/entrypoint.sh` to run `npm start`; and
    -   Configure the `API_SERVER` value so it defaults to `localhost:10080` unless an alternate host
        is provided.
        
    _You may need to modify existing directives as well as add new instructions._

-   Rebuild your image:

    ```bash
    docker build -t petstore:api .
    ```

**Your container should continue to start the application by default, but should also be able to run
other commands if a user provides them to `docker run`. The `swagger.yaml` file should be updated
every time the container runs.**

## Testing

-   Run your container on port `10080` and verify that "Try it out!" for the `getPetById` method
    uses `localhost:10080`:
    1.  Run your container.
    
        ```bash
        docker run --rm -p 10080:8080 petstore:api
        ```
    1.  Open <http://localhost:10080/docs/#!/pet/getPetById>
    
        You should see a screen similar to:
        ![getPetById Docs Screen Shot](img/get_pet_by_id.png)
    1.  Use the "Try it out!" feature to find Pet #1
    1.  Verify that the  `Curl` and `Request URL` sections point to `http://localhost:10080`
        ![API Docs with correct URL, localhost:10080](img/try_it_10080.png)

-   Run your container on port `12345` and configure it to use `localhost:12345` as the URL. Verify that
    "Try it out!" displays `http://localhost:12345`:
    1.  Run your container. Add an appropriate `-e` option to change the server URL.
    
        ```bash
        docker run --rm -p 12345:8080 [OPTIONS] petstore:api
        ```
    1.  Open <http://localhost:12345/docs/#!/pet/getPetById>
    
        You should see a screen similar to:
        ![getPetById Docs Screen Shot](img/get_pet_by_id.png)
    1.  Use the "Try it out!" feature to find Pet #1
    1.  Verify that the  `Curl` and `Request URL` sections point to `http://localhost:12345`
        ![API Docs with correct URL, localhost:12345](img/try_it_12345.png)

-   Run your container with a non-default command and change the URL to `my.server.com`; verify
    that `swagger.yaml` is updated.
    1.  Run your container and `grep` swagger.yaml. Add an appropriate `-e` option to change the server URL.
    
        ```bash
        docker run --rm [OPTIONS] petstore:api grep "host:" /petstore/api/swagger.yaml
        ```
    1.  Verify the output is:
    
        ```
        host: my.server.com
        ```
