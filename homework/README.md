## Goals
1. Learn what is Docker and why I should us it?
1. Install Docker on your machine
1. Become familiar with Docker CLI
1. Understand Docker Image names

## Little Reading
[What is Docker? Pure awesomeness!](https://drewmcmurry.com/kitt/blog/2016/06/03/what-is-docker/?hootPostID=be140ff2cace1958ad6c3f358064634a)

## Install Docker
Mac - <https://docs.docker.com/docker-for-mac/>

Windows - <https://docs.docker.com/docker-for-windows/>

Linux - <https://docs.docker.com/engine/installation/>

## Exercises

### Search the Docker Registry
Reference: <https://docs.docker.com/engine/reference/commandline/search/>

Usage: `docker search NAME'
1. Search for nginx: `docker search nginx`

    ```shell
docker search nginx
NAME                      DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
nginx                     Official build of Nginx.                        5275      [OK]       
jwilder/nginx-proxy       Automated Nginx reverse proxy for docker c...   943                  [OK]
richarvey/nginx-php-fpm   Container running Nginx + PHP-FPM capable ...   342                  [OK]
million12/nginx-php       Nginx + PHP-FPM 5.5, 5.6, 7.0 (NG), CentOS...   75                   [OK]
webdevops/php-nginx       Nginx with PHP-FPM                              69                   [OK]
maxexcloo/nginx-php       Framework container with nginx and PHP-FPM...   58                   [OK]
bitnami/nginx             Bitnami nginx Docker Image                      23                   [OK]
evild/alpine-nginx        Minimalistic Docker image with Nginx            12                   [OK]
million12/nginx           Nginx: extensible, nicely tuned for better...   9                    [OK]
gists/nginx               Nginx on Alpine                                 8                    [O
    ```
2. You can also search for images on [Docker Hub](https://hub.docker.com/). Docker Hub is the offical Docker image registry.


### List Tags for an image
1. Hit the following url in a browser, curl, or wget: <https://registry.hub.docker.com/v1/repositories/nginx/tags>

    ```shell
curl https://registry.hub.docker.com/v1/repositories/nginx/tags
[{"layer": "b4efbc78", "name": "1.10.0"}, {"layer": "0d5491e4", "name": "1.10.0-alpine"}, {"layer": "02a791aa", "name": "1.7"},
 {"layer": "61e8f94e", "name": "1.7.1"}, {"layer": "3f72b0ae", "name": "1.7.10"}, {"layer": "224873bd", "name": "1.7.11"}, 
 {"layer": "02a791aa", "name": "1.7.12"}, {"layer": "d2d79aeb", "name": "1.7.5"},{"layer": "561ed495", "name": "1.7.6"},
 {"layer": "3535cfc9", "name": "1.7.7"}, {"layer": "e46b3488", "name": "1.7.8"}, {"layer": "4b5657a3", "name": "1.7.9"}, 
 {"layer": "2fde8ab4", "name": "1.8"}, {"layer": "1f1c23fb", "name": "1.8-alpine"}, {"layer": "2fde8ab4", "name": "1.8.1"}, 
 {"layer": "1f1c23fb", "name": "1.8.1-alpine"}, {"layer": "0e181a34", "name": "1.9"}, {"layer": "b1e5f40a", "name": "1.9-alpine"}, 
 {"layer": "7f03de0e", "name": "1.9.0"}, {"layer": "fcf2ab26", "name": "1.9.1"}, {"layer": "2b1e900b", "name": "1.9.10"}, 
 {"layer": "54c91435", "name": "1.9.11"}, {"layer": "36b0adef", "name": "1.9.12"}, {"layer": "c0e6aba9", "name": "1.9.14"}, 
 {"layer": "15d695f2", "name": "1.9.14-alpine"}, {"layer": "0e181a34", "name": "1.9.15"}, {"layer": "b1e5f40a", "name": "1.9.15-alpine"}, 
 {"layer": "ce293cb4", "name": "1.9.2"}, {"layer": "6886fb5a", "name": "1.9.3"}, {"layer": "0b354d33", "name": "1.9.4"}, 
 {"layer": "3669a1f1", "name": "1.9.5"}, {"layer": "bf727fd7", "name": "1.9.6"}, {"layer": "ed0c44ad", "name": "1.9.7"}, 
 {"layer": "6ffc0208", "name": "1.9.8"}, {"layer": "6bd8695f", "name": "1.9.9"}]
    ```
2. [Docker Hub](https://hub.docker.com/) will also show the available tags for [nginx](https://hub.docker.com/_/nginx/)

### Pull an image from the public Docker registry
Reference: <https://docs.docker.com/engine/reference/commandline/pull/>

Usage: `docker pull NAME[:TAG]`

1. Pull the latest nginx image: `docker pull nginx` or `docker pull nginx:latest`
2. Pull a particular version of nginx: `docker pull nginx:1.11.9`

### List images on your machine

Reference: <https://docs.docker.com/engine/reference/commandline/images/>

Usage: `docker images`
1. List images on your machine: `docker images`
2. You should see two images listed:

```shell
docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
nginx               1.11.9              cc1b61406712        12 days ago         181.8 MB
nginx               latest              cc1b61406712        12 days ago         181.8 MB
```

### Run a container
Reference: <https://docs.docker.com/engine/reference/commandline/run/>

Usage: `docker run --name NAME|ID -d -p HOST_PORT:CONTAINER_PORT IMAGE_NAME:TAG`

1. Run the nginx container: `docker run --name nginx -d -p 8080:80 nginx`
  * `--name` names the container.  If `--name` is not provided a random unique name is generated
  * `-d` runs the container in the background
  * `-p 8080:80` maps port 8080 of the host to port 80 of the container
2. Verify the nginx container is running properly by hitting <http://localhost:8080> from your browser

### List running containers
Reference: <https://docs.docker.com/engine/reference/commandline/ps/>

Usage: `docker ps`
1. List running containers: `docker ps`
2. You should see your nginx container running:

```shell
docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                           NAMES
7776629a5d9d        nginx               "nginx -g 'daemon off"   9 seconds ago       Up 8 seconds        443/tcp, 0.0.0.0:8080->80/tcp   nginx
```

### Inspect a container
Reference: <https://docs.docker.com/engine/reference/commandline/inspect/>

Usage: `docker inspect NAME|ID`
1. Inspect the nginx container: `docker inspect nginx`
2. You will see lots of information about your container

### Stop a running container
Reference: <https://docs.docker.com/engine/reference/commandline/stop/>

Usage: `docker stop NAME|ID`
1. Stop the nginx container: `docker stop nginx`
2. Verify the nginx container is not running
  * List running containers: `docker ps`
  * There should not be any running containers

    ```shell
    docker ps
    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
    ```

3. Verify nginx is not running by hitting `localhost:8080` from your browser.  Make sure you do a refresh because your browser may cache the web page
4. List all containers including stopped containers:  `docker ps -a`
5. You should see your nginx container but notice the status is Exited

```shell
docker ps -a
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                     PORTS               NAMES
7776629a5d9d        nginx               "nginx -g 'daemon off"   15 minutes ago      Exited (0) 5 minutes ago                       nginx
```

### Start a stopped container
Reference: <https://docs.docker.com/engine/reference/commandline/start/>

Usage: `docker start NAME|ID`
1. Start the stopped nginx container: `docker start nginx`
2. Verify nginx is running
  * List running container: `docker ps`
  * Hit `localhost:8080` from a browser

### Run a command in a running container
Reference: <https://docs.docker.com/engine/reference/commandline/exec/>

Usage: `docker exec [OPTIONS] CONTAINER COMMAND [ARG...]`
1. List the files in the nginx container: `docker exec nginx ls -al`
2. You should see a list of files in your terminal:

    ```shell
docker exec nginx ls -al
total 72
drwxr-xr-x 35 root root 4096 Feb  6 04:09 .
drwxr-xr-x 35 root root 4096 Feb  6 04:09 ..
-rwxr-xr-x  1 root root    0 Feb  6 03:43 .dockerenv
drwxr-xr-x  2 root root 4096 Jan 16 18:05 bin
drwxr-xr-x  2 root root 4096 Dec 28 17:42 boot
drwxr-xr-x  5 root root  360 Feb  6 04:09 dev
drwxr-xr-x 58 root root 4096 Feb  6 03:43 etc
drwxr-xr-x  2 root root 4096 Dec 28 17:42 home
drwxr-xr-x 10 root root 4096 Jan 24 18:51 lib
drwxr-xr-x  2 root root 4096 Jan 16 18:03 lib64
drwxr-xr-x  2 root root 4096 Jan 16 18:02 media
drwxr-xr-x  2 root root 4096 Jan 16 18:02 mnt
drwxr-xr-x  2 root root 4096 Jan 16 18:02 opt
dr-xr-xr-x 89 root root    0 Feb  6 04:09 proc
drwx------  2 root root 4096 Jan 16 18:02 root
drwxr-xr-x  3 root root 4096 Feb  6 04:09 run
drwxr-xr-x  2 root root 4096 Jan 16 18:05 sbin
drwxr-xr-x  2 root root 4096 Jan 16 18:02 srv
dr-xr-xr-x 12 root root    0 Feb  6 04:09 sys
drwxrwxrwt  2 root root 4096 Jan 24 18:52 tmp
drwxr-xr-x 15 root root 4096 Jan 24 18:51 usr
drwxr-xr-x 16 root root 4096 Feb  6 03:43 var    
    ```
    
### Create a Bash session in a running container
1. Start a bash shell in the nginx container: ` docker exec -it nginx bash`
  * `-i` interactive mode
  * `-t` create a pseudo terminal
2. Now you can run any commands you want inside your container
3. List the files in the container: `ls -al`

## Image Names

A Docker Image Name can contain as many as four parts:

1.  Registry URL
2.  Image Namespace
3.  Image Name
4.  Image Tag

The only required component is the Image Name. If the Registry URL is missing, Docker looks
for the image on [Docker Hub](https://hub.docker.com). Images without a namespace are "library"
images that are maintained by Docker. If you don't provide an Image Tag, Docker will look for
an image with the default tag `latest`.

### Image Name Format

`[<url>/][<namespace>/]<name>[:<tag>]`

### Example Names

```
# Docker's latest docker library image
docker

# Explicitly referencing the latest docker image
docker:latest

# Locking to docker version 1.13
docker:1.13

# Docker's Docker-in-Docker image, that supports building other images within a running container
docker:dind

# GitLab's official GitLab CI Runner image
gitlab/gitlab-runner:v1.11.0

# An awesome image hosted on BTI's (fictional) private registry
registry.bti360.com/bti360/awesome-image:latest
```

### Pull an Image from a Private Registry

Use `docker pull` to download the image with the following components:

| Component      | Value                 |
|----------------|-----------------------|
| `Registry URL` | `registry.gitlab.com` |
| `Namespace`    | `bti360-kata`         |
| `Name`         | `node`                |
| `Tag`          | `6-alpine`            |
