# BTI360 Docker Kata

Code samples, prerequisites, and documentation for the March 2017 Kata on Docker for BTI360's Orange
Slice.

Go to the [Wiki](https://gitlab.com/bti360-kata/docker-intro/wikis/Home) or work through the
[Homework](https://gitlab.com/bti360-kata/docker-intro/wikis/Homework) to get started!
