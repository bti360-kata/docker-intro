# Before we begin...

1.  Open this page in your browser: <https://gitlab.com/bti360-kata/docker-intro/wikis/Home>

1.  Install Docker: <https://www.docker.com/products/overview>

1.  Clone the Kata Code:

    ```bash
    $ git clone https://gitlab.com/bti360-kata/docker-intro.git
    ```
    
1.  Open a terminal in the `docker-intro` directory.

1.  Download and alias the Docker image.

    ```bash
    $ docker pull registry.gitlab.com/bti360-kata/node:6-alpine
    $ docker tag registry.gitlab.com/bti360-kata/node:6-alpine kata:node6
    ```

1.  Run a NodeJS command inside the Docker container.

    ```bash
    $ docker run -it kata:node6
    > console.log('Hello, Docker!')
    ```
